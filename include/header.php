<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-custom">
  <a class="navbar-brand" href=""><img src="img/kingsman.png" style="width:30px"></img></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="/kingsman" name="scroller">WELCOME<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="blog">BLOG</a>
      </li>
    </ul>
    <ul class="navbar-nav pull-right">
<?php if(!isset($_SESSION['user_loggedcode000'])){ ?>
      <li class="nav-item">
        <a class="nav-link" href="login">Log In</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" style="cursor:pointer" data-toggle="modal" data-target="#register">Register</a>
      </li><?php }else{?>
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo $_SESSION['user_loggedcode000'];?>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="dashboard">Dashboard</a>
        <a class="dropdown-item" href="data/logout.php">Logout</a>
      </div>
      </li> <?php }?>
    </ul>
  </div>
</nav>
