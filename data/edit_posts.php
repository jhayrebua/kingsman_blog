<?php
require_once('../class/c_blog.php');
if(isset($_POST['uniq'])){
  $uniq = $_POST['uniq'];
  $userid = $_POST['userid'];
  $list = $blog->editPost($uniq,$userid);
 ?>
  <?php if(count($list) > 0){ ?>
    <?php foreach($list as $row): ?>

        <div class="form-group row">
          <div class="col-sm-12">
            <h2 class="font">Edit Posts</h2>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12">
            <h4 class="font">Title</h4>
            <input type="text" id="edit_title" class="form-control" maxlength="50" value="<?= $row['title']; ?>" required>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12">
            <h4 class="font">Body</h4>
            <textarea id="edit_body" name="edit_body" rows="8" cols="80"class="form-control" required maxlength="300"><?= $row['body']; ?></textarea>
          </div>
        </div>
        <input type="hidden" id="edit_coverimage" value="<?= $row['cover_image'] ?>">
        <input type="file" name="edit_file" id="edit_file" accept="image/png,image/jpeg">
        <br/>
        <br/>
        <input type="submit" value="Submit" class="btn btn-primary">

    <?php endforeach; ?>
<?php
    }//if
}//isset ?>
