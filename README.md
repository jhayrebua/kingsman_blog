# Kingsman Theme blog *v2*

## VERSION
  ```
  v1.01 Added comment section.
  ```
  ```
  v1.0 This version only allows user to create a posts/edit and delete.
  ```

## Requirements
  ```
  xampp/lampp
  browser
  ```

## Installation
  ```
  Extract kingsman_blog.zip.
  Import kingsman.sql to your database.
  Copy the whole folder to your htdocs(lampp and xampp).
  ```
  
## How to use
  ```
  After installation you can access the page from localhost/kingsman
  ```
  
  
  
