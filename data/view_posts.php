<?php
require_once('../class/c_blog.php');
require_once('../class/c_comments.php');
if(isset($_POST['uniq'])){
  $uniq = $_POST['uniq'];
  $list = $blog->viewPosts($uniq);
  $comments = $comment->listComment($uniq);
 ?>
  <?php if(count($list) > 0){ ?>
    <?php foreach($list as $row): ?>
      <br/>
      <div class="card border-warning bg-secondary" width="100%">
        <div class="card-body" >
          <div class="form-group row">
            <div class="col-sm-12">
              <h2><strong><?= $row['title']; ?></strong></h2>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-12">
              <img src="coverimg/<?= $row['cover_image']; ?>" alt="..." style="width:30vh; height:30vh;">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-12">
              <p class="body-text"><?= $row['body']; ?></p>
              <small>Created at <?= $row['created_at']; ?> by <?= $row['created_by']; ?></small>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" id="post_id" value="<?= $row['id']; ?>">
    <?php endforeach; ?>
    <br/>
    <h5>Comment Section (<?php echo count($comments)?>)</h5>
<?php
    }//if?>
    <?php if(count($comments) > 0){ ?>
      <?php foreach($comments as $row): ?>
        <div class="form-group row">
          <div class="col-sm-6">
            <div class="card border-warning ">
              <div style="margin:5px;">
                <p class="body-text"><?= $row['comment'] ?></p>
                <small><i><?= $row['comment_by'] ?> commented on <?= $row['created_at'] ?></i> </small>
                <?php if(isset($_SESSION['user_id'])){ ?>
                  <?php if($_SESSION['user_id'] === $row['user_id']){ ?>
                    <a href="data/delete_comment?id=<?php echo $row['id'] ?>&idu=<?= $row['user_id'] ?>&security=<?php echo '0008118'  ?>&postid=<?= $row['post_id'] ?>">&nbsp; <u>Delete this comment</u></a>
                  <?php } //if ?>
                <?php } //sisset?>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    <?php } //if ?>
<?php }//isset ?>
