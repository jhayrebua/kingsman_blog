<?php
interface i_blog{
  public function postlist($uniq); //post by user
  public function allpostlist(); //all row
  public function createPost($user_id,$title,$body,$created_at,$created_by,$cover_image); //CREATE PPOST
  public function editPost($uniq,$user);//post to be edit // ADDED USER ID TO PREVENT OTHERS FROM BYPASSING EDITING POSTS FROM OTHERS
  public function viewPosts($uniq); //VIEW POST ONLY
  public function updatePost($uniq,$user_id,$title,$body,$cover_image); //UPDATE
  public function deletePost($uniq,$user_id); //DELETE
}  
 ?>
