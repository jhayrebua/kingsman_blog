<?php
require_once('../database/database.php');
require_once('../interface/i_comments.php');

class Comments extends database implements i_comments{

  public function addComment($post_id,$comment,$comment_by,$user_id,$created_at){
    $query = "INSERT INTO comments(post_id,comment,comment_by,user_id,created_at) values(?,?,?,?,?)";
    $type = 'issss';
   return $this->insertRow($query,$type,[$post_id,$comment,$comment_by,$user_id,$created_at]);
  }

  public function listComment($post_id){
    $query = "SELECT * FROM comments WHERE post_id = ? ORDER BY created_at DESC";
    $type = "i";
    return $this->getRow($query,$type,[$post_id]);
  }

  public function deleteComment($user_id,$id){
    $query = "DELETE FROM comments WHERE user_id = ? and id = ?";
    $type = "ii";
    return $this->deleteRow($query,$type,[$user_id,$id]);
  }

}

$comment = new Comments();
 ?>
