<?php
require_once('../class/c_blog.php');

if(isset($_POST['title'])){
  date_default_timezone_set('Asia/Singapore');//get created at
  $title = $_POST['title'];
  $body = $_POST['body'];
  $uniq = $_POST['uniq']; //user id
  $prev_image = $_POST['prev_image'];
  $id = $_POST['id']; //text id

  if($_FILES["image"]["name"] != ''){
    $file = explode(".",$_FILES["image"]["name"]);
    $extension  = end($file);
    $new = date("mdYhis");
    $cover_image = $title."@".$new.".".$extension;
    $location = '../coverimg/'.$cover_image;
    move_uploaded_file($_FILES["image"]["tmp_name"],$location);
    if($prev_image!="noimage.jpg")
      unlink('../coverimg/'.$prev_image);
  }else {
    $cover_image = $prev_image;
  }

  $edit = $blog->updatePost($id,$uniq,$title,$body,$cover_image);

  if($edit === true){
    $_SESSION['alert'] = "success";
    //$return['msg'] = "added";
  }else {
    //$return['msg'] = "failed";
    $_SESSION['alert'] = "error";
  }
  //echo json_encode($return);
}//isset

?>
