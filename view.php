<!DOCTYPE html>
<html lang="en">
<?php session_start();?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
<br/>
  <div class="container">
    <br/>
    <button type="button" class="btn btn-primary" onclick="location.href='blog'">Go back</button>
    <div id="view_div">
    </div>
    <?php if(isset($_SESSION['user_loggedcode000'])){?>
      <form id="comment_section" method="post">
        <div class="form-group row">
          <div class="col-sm-6">
            <textarea name="name" rows="2" cols="80" id="comment" class="form-control" placeholder="Enter comment" required></textarea>
          </div>
        </div>
        <div class="text-left">
          <input type="submit" name="" value="Add comment" class="btn btn-primary">
        </div>
      </form>
    <?php }else {?>
      <h3>Login to comment</h3>
    <?php } ?>
  </div><!-- container-->
  <input type="hidden" id="hidden_uniq" value="<?php echo $_SESSION['user_id'];?>"><!-- hidden user id-->
  <input type="hidden" id="hidden_name" value="<?php echo $_SESSION['user_loggedcode000'];?>"><!-- hidden user-->
  <input type="hidden" id="hidden_id" value="<?php echo $_GET['id']?>"><!-- hidden text id-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
  <?php include('modal/register.html');?>
</body>
</html>
