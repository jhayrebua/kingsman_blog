<?php
require_once('../database/database.php');
require_once('../interface/i_user.php');

class User extends database implements i_user{

  public function login($username,$password){
    $query = "SELECT * FROM users WHERE username =  ? and password = ?";
    $type = "ss";
    return $this->getRow($query,$type,[$username,$password]);
  }

  public function register($username,$password,$email){
    $query = "INSERT INTO users(username,password,email) values(?,?,?)";
    $type = "sss";
    return $this->insertRow($query,$type,[$username,$password,$email]);
  }

}//end class

$user = new User();

 ?>
