<?php
require_once('../class/c_comments.php');

if(isset($_POST['post_id'])){
  $post_id = $_POST['post_id'];
  $name = $_POST['name'];
  $commento = $_POST['comment'];
  $user_id = $_POST['user_id'];
  date_default_timezone_set('Asia/Singapore');//get created at
  $created_at = date("m-d-Y H:i:s");

  $result = $comment->addComment($post_id,$commento,$name,$user_id,$created_at);
  if($result === true){
    $_SESSION['alert'] = "success";
  }else {
    $_SESSION['alert'] = "error";
  }

}//
