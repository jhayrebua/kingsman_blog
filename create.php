<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
<hr>
  <div class="container">
    <br/>
    <form id="create" method="POST">
      <div class="form-group row">
        <div class="col-sm-12">
          <button type="button" class="btn btn-primary" onclick="location.href='dashboard'">Go back</button>
          <h2 class="font">Create Posts</h2>
        </div>
      </div>
      <?php include('include/alerts.php');?>
      <div class="form-group row">
        <div class="col-sm-12">
          <h4 class="font">Title</h4>
          <input type="text" id="create_title" class="form-control" maxlength="50" required>
        </div>
      </div>
      <div class="form-group row">
        <div class="col-sm-12">
          <h4 class="font">Body</h4>
          <textarea id="create_body" name="create_body" rows="8" cols="80" class="form-control" required maxlength="300"></textarea>
        </div>
      </div>
      <input type="file" name="create_file" id="create_file" accept="image/png,image/jpeg">
      <br/>
      <br/>
      <input type="submit" value="Submit" class="btn btn-primary">
    </form>
  </div><!-- container-->
  <input type="hidden" id="hidden_uniq" value="<?php echo $_SESSION['user_id'];?>"><!-- hidden user id-->
  <input type="hidden" id="hidden_name" value="<?php echo $_SESSION['user_loggedcode000'];?>"><!-- hidden text user-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
</body>
</html>
