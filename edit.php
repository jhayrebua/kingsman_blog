<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
<br/>
  <div class="container">
    <br/>
    <button type="button" class="btn btn-primary" onclick="location.href='dashboard'">Go back</button>
    <form id="editform" method="POST">
      <div id="edit_div">
      </div>
    </form>
  </div><!-- container-->
  <input type="hidden" id="hidden_uniq" value="<?php echo $_SESSION['user_id'];?>"><!-- hidden user id-->
  <input type="hidden" id="hidden_name" value="<?php echo $_SESSION['user_loggedcode000'];?>"><!-- hidden user-->
  <input type="hidden" id="hidden_id" value="<?php echo $_GET['id']?>"><!-- hidden text id-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
</body>
</html>
