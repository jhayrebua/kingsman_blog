<!DOCTYPE html>
<html lang="en">
<?php session_start();
include('include/head.html'); ?>
<body>
  <?php include('include/header.php'); ?>
<br/>
  <div class="container">
    <br/>
    <div class="form-group row">
      <div class="col-sm-12">
        <h2 class="font">BLOG POSTS</h2>
      </div>
    </div>
    <hr>
    <div id="blog_div">
    </div>
  </div><!-- container-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
  <?php include('modal/register.html');?>
</body>
</html>
