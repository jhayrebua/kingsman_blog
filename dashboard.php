<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
<br/>
  <div class="container">
    <br/>
    <div class="form-group row">
      <div class="col-sm-10">
        <h2 class="font">Dashboard</h2>
      </div>
      <div class="col-sm-2">
        <button type="button" onClick="location.href='create'" class="form-control btn btn-primary">Add Posts</button>
      </div>
    </div>
    <hr>
    <?php include('include/alerts.php');?>
    <div id="dashboard_div">
    </div>
  </div><!-- container-->
  <input type="hidden" id="hidden_uniq" value="<?php echo $_SESSION['user_id'];?>"><!-- hidden text id-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
</body>
</html>
