<?php
require_once('../class/c_blog.php');
  //$uniq = $_POST['uniq'];
  $list = $blog->allpostlist();
 ?>

  <?php if(count($list) > 0){ ?>
    <?php foreach($list as $row): ?>
        <div class="form-group row" name="page">
          <div class="col-sm-12">
            <div class="card border-warning mb-3 bg-secondary" width="100%">
              <div class="card-body" style="color:white;">
                <div class="form-group row">
                  <div class="col-sm-3">
                    <img src="coverimg/<?= $row['cover_image'] ?>" alt="" style="width:20vh; height:21vh;">
                  </div>
                  <div class="col-sm-9">
                    <h3 class="card-title"><a href="view?id=<?= $row['id'] ?>" style="color:white;"><?= $row['title']; ?></a></h3>
                    <small>Created at <?= $row['created_at'];?> by <?= $row['created_by']; ?></small>
                    <br/>
                    <?php if(isset($_SESSION['user_id'])){ ?>
                      <?php  if($_SESSION['user_id'] == $row['user_id']){ ?>
                        <button type="button" onclick="location.href='edit?id=<?php echo $row['id']; ?>'" class="btn btn-primary">Edit</button>
                        <button type="button" onclick="location.href='data/delete?id=<?php echo $row['id'] ?>&idu=<?= $row['user_id'] ?>&security=<?php echo '0008118'  ?>'" class="btn btn-danger">Delete</button>
                      <?php }
                    }//isset ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <?php endforeach; ?>
  <?php }else{ //end if ?>
    <h3 class="font">No posts found</h3>
  <?php
    }//if
  ?>
