$(document).ready(function(){

  $("#login").submit(function(event){ ///E1 //FOR Login
    event.preventDefault();
    var un = $("#un").val();
    var pw = $("#pw").val();

    $.ajax({
      type:'post',
        url:'data/login.php',
          dataType:'json',
            data:{
              un:un,
              pw:pw
            },
            success: function(data){
              console.log(data);
              if(data.logged == 'true'){
                location.href = data.url;
              }else {
                $.notify(data.msg);
              }
            },
            error: function(data){
              alert("ERROR:E1");
            }
    });
  });//SUBMIT

  $("#register").submit(function(event){ ///EVENT 2 "E2" REGISTER
    event.preventDefault();
    var reg_un = $("#reg_un").val();
    var reg_pw = $("#reg_pw").val();
    var reg_email = $("#reg_email").val();

    $.ajax({
      type:'post',
        url:'data/register.php',
        //  dataType:'json',
            data:{
              reg_un:reg_un,
              reg_pw:reg_pw,
              reg_email:reg_email
            },
            success: function(data){
              console.log(data);
              //$.notify(data.msg);
              location.href='login'
            },
            error:function(data){
              alert("ERROR:E2");
            }
    });
  });


  $("#create").submit(function (event){ //E3
    event.preventDefault();

    var title = $("#create_title").val();
    var body = $("#create_body").val();
    var uniq = $("#hidden_uniq").val();
    var name = $("#hidden_name").val();
    var image =  $("#create_file").get(0).files[0];
    var form_data = new FormData();
      form_data.append("image",image);
      form_data.append("name",name);
      form_data.append("uniq",uniq);
      form_data.append("body",body);
      form_data.append("title",title);

      $.ajax({
        type:'post',
          url:'data/create_posts.php',
            //dataType:'json',
             data:form_data,
              contentType:false,
               cache:false,
                processData:false,
                    success:function(data){
                      console.log(data);
                      //alert(data.msg);
                      location.reload();
                    },
                    error:function(data){
                      alert("ERROR: E3");
                    }
      });
    });

    $("#editform").submit(function (event){ //E4
      event.preventDefault();

      var title = $("#edit_title").val();
      var body = $("#edit_body").val();
      var uniq = $("#hidden_uniq").val();
      var id = $("#hidden_id").val();
      var image =  $("#edit_file").get(0).files[0];
      var prev_image = $("#edit_coverimage").val();
      var form_data = new FormData();
        form_data.append("image",image);
        form_data.append("id",id);
        form_data.append("uniq",uniq);
        form_data.append("body",body);
        form_data.append("title",title);
        form_data.append("prev_image",prev_image);

        $.ajax({
          type:'post',
            url:'data/submit_editposts.php',
              //dataType:'json',
               data:form_data,
                contentType:false,
                 cache:false,
                  processData:false,
                      success:function(data){
                        console.log(data);
                        //alert(data.msg);
                        location.href='dashboard';
                      },
                      error:function(data){
                        alert("ERROR: E4");
                      }
        });
      });

      $("#comment_section").submit(function (event){ //E5
        event.preventDefault();
        var post_id = $("#post_id").val();
        var name = $("#hidden_name").val();
        var comment = $("#comment").val();
        var user_id = $("#hidden_uniq").val();
        $.ajax({
          type: 'post',
            url: 'data/add_comment.php',
              data:{
                post_id:post_id,
                name:name,
                comment:comment,
                user_id:user_id
              },
              success: function(data){
                console.log(data);
                location.reload();
              },
              error:function(data){
                alert("ERROR: E5");
              }
        });
      });

  function dashboard_list(){ //F1 , FETCH DATA POST
    var uniq = $("#hidden_uniq").val();
    $.ajax({
      type:'post',
        url:'data/dashboard_postlist.php',
          data:{
            uniq:uniq
          },
          success:function(data){
            console.log(data);
            $("#dashboard_div").html(data);
            $('#dashboard_div').paginate({
            scope: $('div[name="page"]'), // targets all div elements
            perPage: 3
            });
          },
          error: function(data){
              console.log("ERROR:F1");
          }
    });
  }
  dashboard_list();


  function edit_list(){ //F2 , FETCH DATA edit
    var uniq = $("#hidden_id").val();
    var userid = $("#hidden_uniq").val();
  //  alert(uniq);
    //alert(userid);
    $.ajax({
      type:'post',
        url:'data/edit_posts.php',
          data:{
            uniq:uniq,
            userid:userid
          },
          success:function(data){
            console.log(data);
            $("#edit_div").html(data);
          },
          error: function(data){
              console.log("ERROR:F2");
          }
    });
  }
  edit_list();

  function blog_list(){ //F3 , FETCH DATA POST
    $.ajax({
      type:'post',
        url:'data/blog_postlist.php',
          success:function(data){
            console.log(data);
            $("#blog_div").html(data);
            $('#blog_div').paginate({
            scope: $('div[name="page"]'), // targets all div elements
            perPage: 3
            });
          },
          error: function(data){
              console.log("ERROR:F3");
          }
    });
  }
  blog_list();

  function view_list(){ //F4 , FETCH DATA view
    var uniq = $("#hidden_id").val();
  //  alert(uniq);
    //alert(userid);
    $.ajax({
      type:'post',
        url:'data/view_posts.php',
          data:{
            uniq:uniq
          },
          success:function(data){
            console.log(data);
            $("#view_div").html(data);
          },
          error: function(data){
            console.log("ERROR:F4");
          }
    });
  }
  view_list();
});//END DOCU READY
