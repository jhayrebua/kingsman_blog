<?php
require_once('../database/database.php');
require_once('../interface/i_blog.php');

class Blog extends database implements i_blog{

  public function postlist($uniq){ //post by user
    $query = "SELECT * FROM blog WHERE user_id = ? ORDER BY created_at DESC";
    $type = "i";
    return $this->getRow($query,$type,[$uniq]);
  }
  public function allpostlist(){ //all row
    $query = "SELECT * FROM blog ORDER BY created_at DESC";
    return $this->getallRow($query);
  }

  public function createPost($user_id,$title,$body,$created_at,$created_by,$cover_image){
    $query = "INSERT INTO blog(user_id,title,body,created_at,created_by,cover_image) values(?,?,?,?,?,?)";
    $type = "isssss";
    return $this->insertRow($query,$type,[$user_id,$title,$body,$created_at,$created_by,$cover_image]);
  }

  public function editPost($uniq,$user){ //post to be edit // ADDED USER ID TO PREVENT OTHERS FROM BYPASSING EDITING POSTS FROM OTHERS
    $query = "SELECT * FROM blog WHERE id = ? and user_id = ?";
    $type = "ii";
    return $this->getRow($query,$type,[$uniq,$user]);
  }

  public function viewPosts($uniq){ //post to be edit // ADDED USER ID TO PREVENT OTHERS FROM BYPASSING EDITING POSTS FROM OTHERS
    $query = "SELECT * FROM blog WHERE id = ?";
    $type = "i";
    return $this->getRow($query,$type,[$uniq]);
  }

  public function updatePost($uniq,$user_id,$title,$body,$cover_image){
    $query = "UPDATE blog set title = ?, body = ? , cover_image = ? WHERE user_id = ? and id = ?";
    $type = "sssii";
    return $this->updateRow($query,$type,[$title,$body,$cover_image,$user_id,$uniq]);
  }

  public function deletePost($uniq,$user_id){
    $query = "DELETE FROM blog WHERE user_id = ? and id = ?";
    $type = "ii";
    return $this->deleteRow($query,$type,[$user_id,$uniq]);
  }

}//end class

$blog = new blog();

 ?>
