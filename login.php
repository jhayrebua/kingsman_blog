<!DOCTYPE html>
<html lang="en">
<?php include('include/head.html');
session_start();
?>
<body>
<?php include('include/header.php'); ?>
  <div class="container">
    <br/>
    <br/>
      <div class="text-center">
        <h2 class="font">WELCOME TO LOGIN SECTION</h2>
      </div>
    <form class="form-horizontal" id="login" method="post" action="">
      <div class="form-group row">
        <div class="col-sm-6">
          <img src="img/kingsman_front.jpg" width="100%" alt="....">
        </div>
        <div class="col-sm-6">
          <div class="form-group row">
            <div class="col-sm-5"></div>
            <div class="col-sm-7">
              <?php include('include/alerts.php');?>
              <label class="col-form-label">Username: </label>
              <input type="text" id="un" class="form-control input input-lg" maxlength="12" placeholder="Username" autofocus required>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-5"></div>
            <div class="col-sm-7">
              <label class="col-form-label">Password: </label>
              <input type="password" id="pw" class="form-control input-lg" placeholder="Password" required>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9">
            </div>
            <div class="col-sm-3">
                <input type="submit" name='submit' class="form-control btn btn-primary " value="Login">
                <!--<button type="button" name="button" id="login" class="form-control btn btn-primary">Login</button>-->
            </div>
          </div>
        </div>
      </div>
    </form>
  </div><!-- container-->
  <footer class="container text-muted">
    <div class="row">
      <div class="col-sm-12">
        <hr>
        <p class="text-center">&copy; Jesson Jei Rebua</p>
      </div>
    </div>
  </footer>
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
  <?php include('modal/register.html');?>
</body>
</html>
