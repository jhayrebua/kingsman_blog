<!DOCTYPE html>
<html lang="en">
<?php session_start();
$_SESSION['alert'] = "";
?>
<?php include('include/head.html'); ?>
<body>
  <?php include('include/header.php'); ?>
  <a id="welcome" name="welcome"></a>
  <div class="container">
    <br/>
    <br/>
    <div class="text-center">
      <img src="img/title.png" alt="" style="width:40vh; height:10vh;" class="animated bounceInLeft">
    </div>
    <div id="cast" class="carousel slide" data-ride="carousel" data-interval="2000">
      <ol class="carousel-indicators">
        <li data-target="#cast" data-slide-to="0" class="active"></li>
        <li data-target="#cast" data-slide-to="1"></li>
        <li data-target="#cast" data-slide-to="2"></li>
        <li data-target="#cast" data-slide-to="3"></li>
        <li data-target="#cast" data-slide-to="4"></li>
        <li data-target="#cast" data-slide-to="5"></li>
        <li data-target="#cast" data-slide-to="6"></li>
        <li data-target="#cast" data-slide-to="7"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="img/eggsy.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/harry.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/merlin.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/tequila.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/jeff.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/whiskey.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/ginger.jpg" alt="Third slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/poppy.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#cast" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#cast" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <br/>
    <div id="accordion" role="tablist">
      <div class="card">
        <div class="card-header" role="tab" id="headingThree">
          <h5 class="mb-0">
            <a class="collapsed" data-toggle="collapse" href="#casts" aria-expanded="false" aria-controls="collapseThree">
              Cast
            </a>
          </h5>
        </div>
        <div id="casts" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <ul>
                  <li>Colin Firth as Harry Hart / Galahad</li>
                  <li>Samuel L. Jackson as Richmond Valentine</li>
                  <li>Mark Strong as Merlin</li>
                  <li>Taron Egerton as Gary "Eggsy" Unwin</li>
                </ul>
              </div>
              <div class="col-sm-4">
                <ul>
                  <li>Michael Caine as Chester King / Arthur</li>
                  <li>Sophie Cookson as Roxanne Morton</li>
                  <li>Sofia Boutella as Gazelle</li>
                  <li>Samantha Womack as Michelle Unwin</li>
                </ul>
              </div>
              <div class="col-sm-4">
                <ul>
                  <li>Geoff Bell as Dean Baker</li>
                  <li>Edward Holcroft as Charles Hesketh</li>
                  <li>Mark Hamill as Professor James Arnold</li>
                  <li>Jack Davenport as James Spencer</li>
                </ul>
              </div>
            </div>
            <p style="text-align:justify">
              Hanna Alström and Bjørn Floberg appear as Crown Princess Tilde of Sweden, and Swedish Prime Minister Morten Lindström, respectively. Jack Cutmore-Scott portrays Rufus Saville, and Lily Travers portrays Lady Sophie. Jonno Davies played Lee Unwin, Eggsy's father and a former Kingsman candidate who sacrificed himself to save Hart. Nicholas Banks, Nicholas Agnew, Rowan Polonski and Tom Prior portrayed, respectively, Digby Barker, Nathaniel, Piers and Hugo Higins, the other four Kingsman candidates. Fiona Hampton played Amelia, a Kingsman employee who masquerades as a candidate in order to "die" during the first test. Richard Brake played the interrogator during the penultimate test, Ralph Ineson the police interviewer after Eggsy's arrest, whereas Corey Johnson starred as a fanatic church leader, and Velibor Topić portrayed the biggest goon in the bar fight scene. Tobias Bakare and Theo Barklem-Biggs play Eggsy's friends Jamal and Ryan.
            </p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingOne">
          <h5 class="mb-0">
            <a data-toggle="collapse" href="#plot" aria-expanded="true" aria-controls="collapseOne">
              Plot
            </a>
          </h5>
        </div>
        <div id="plot" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            <p style="text-align:justify">
              During a mission in the Middle East in 1997, probationary secret agent Lee Unwin sacrifices himself to protect his superior Harry Hart. Hart, blaming himself, returns to London to give Lee's widow Michelle and her young son Gary "Eggsy" a medal engraved with an emergency assistance number.
              Seventeen years later, Eggsy, having dropped out of training for the Royal Marines despite his intelligence and capability, has become a stereotypical chav.[8][9] After being arrested for stealing a car, Eggsy calls the number. Hart arranges his release. Hart explains that he is a member of Kingsman, a private intelligence service founded by wealthy British individuals who lost their heirs in World War I. Hart, code name "Galahad", explains there is a position available, as agent "Lancelot" was killed by the assassin Gazelle while trying to rescue Professor James Arnold from kidnappers. Eggsy becomes Hart's candidate. Other candidates are eliminated through dangerous training tests run by operative "Merlin", until only Eggsy and Roxy, a candidate he befriended, are left. Eggsy is unable to complete the final test – shooting a dog he has raised during the training process (unaware that the gun holds blanks) – and Roxy is named the new "Lancelot".
              Meanwhile, Merlin discovers that Professor Arnold has returned to work as if nothing had happened. Hart attempts to interrogate him, but a chip in Professor Arnold's neck explodes, killing him. The detonation signal is traced to a facility owned by Internet billionaire and philanthropist Richmond Valentine, who has recently offered everyone in the world SIM cards that grant free lifetime cellular and Internet connectivity. Hart, impersonating a billionaire philanthropist, meets Valentine face-to-face. Hart learns of Valentine's connection to an obscure hate group's church in Kentucky, and travels there, wearing glasses containing a video transceiver. Eggsy watches as Valentine activates the SIM cards in the church, triggering a signal that causes the parishioners to become murderously violent. Hart's spy training leaves him as the only survivor. Outside the church Valentine explains what happened, then shoots Hart in the face.
              Eggsy returns to Kingsman headquarters and finds that Chester "Arthur" King, Kingsman's leader, has a scar on his neck just like Professor Arnold's. King reveals that Valentine plans to transmit his "neurological wave" worldwide via satellite network, believing the resulting "culling" of the human race will avert its extinction. Only those Valentine has chosen, willing and unwilling, will be unaffected. King tries to poison Eggsy, but Eggsy switches glasses and King poisons himself.
              Eggsy, Merlin and Roxy set out to stop Valentine. Roxy uses high-altitude balloons to destroy one of Valentine's satellites and break up the network, but Valentine quickly secures a replacement from a business associate. Merlin flies Eggsy to Valentine's base, where he masquerades as King. Eggsy is discovered by a failed Kingsman recruit, Charlie Hesketh, leading to both Eggsy and Merlin being cornered. On Eggsy's suggestion, Merlin activates the implanted chips' failsafe, killing almost everyone with a chip. An angry Valentine activates the signal and triggers worldwide pandemonium. Eggsy kills Gazelle and uses one of her sharpened prosthetic legs to impale Valentine and kill him, stopping the signal and ending the threat.
              In a mid-credits scene, Eggsy, now a full Kingsman agent, offers his mother and half-sister a new home away from his abusive stepfather Dean, who flatly objects to Eggsy's suggestion. Eggsy then dispatches him in exactly the same manner that Hart dealt with one of Dean's henchmen earlier.
            </p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingTwo">
          <h5 class="mb-0">
            <a class="collapsed" data-toggle="collapse" href="#production" aria-expanded="false" aria-controls="collapseTwo">
              Production
            </a>
          </h5>
        </div>
        <div id="production" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
          <div class="card-body">
            <p style="text-align:justify">
              The project originated when Mark Millar and Vaughn were at a bar discussing spy movies, lamenting that the genre had become too serious over the years and deciding to do "a fun one." To have the time to make the film, Vaughn had to opt out of directing X-Men: Days of Future Past, which he called "a really tough decision". He reasoned that if he did not do it, "somebody else ... [would] wake up and do a fun spy movie. Then I would have written a bloody screenplay that no one would want to make."Colin Firth joined the cast to lead the film on 29 April 2013. It was initially reported in 2013 that Leonardo DiCaprio was in talks to play a villain, although Vaughn himself later denied that he was ever considered stating that he came as close to playing the role "as I am to becoming the Pope."Instead the role of the villain went to Samuel L. Jackson, Jackson took the role, in part because of a career long dream to be in a James Bond movie. As he felt that this was unlikely to come true he took on the role stating "I felt like this was an opportunity to play a really great Bond villain.”  Jackson's character has a notable lisp, a choice he decided to make based partially on the stutter he had as a kid.In September 2013, Vaughn cast Sophie Cookson for the female lead, preferring a newcomer over more obvious candidates like Emma Watson and Bella Heathcote. Mark Hamill was cast as Professor James Arnold, a reference to his character in the source comic book being named "Mark Hamill".
            </p>
          </div>
        </div>
      </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingOne">
          <h5 class="mb-0">
            <a data-toggle="collapse" href="#about_this_page" aria-expanded="true" aria-controls="collapseOne">
              About this page
            </a>
          </h5>
        </div>
        <div id="about_this_page" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            <p style="text-align:justify">
              This webpage inspired from the movie "KINGSMAN" is created for portfolio purposes only. Using bootstrap 4 , css , javascript for front-end and php OOP for backend.
            </p>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <footer class="footer">
      <p class="copyright" >&copy; Jesson Jei M. Rebua</p>
    </footer>
  </div><!-- container-->
  <?php $i = rand(1,31817); //random to prevent browser frm caching javascript?>
  <script type="text/javascript" src="assets/js_function.js?<?php echo $i;?>"></script>
  <?php include('modal/register.html');?>
</body>
</html>
