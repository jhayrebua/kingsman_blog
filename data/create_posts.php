<?php
require_once('../class/c_blog.php');

if(isset($_POST['title'])){
  date_default_timezone_set('Asia/Singapore');//get created at
  $created_at = date("m-d-Y H:i:s");
  $title = $_POST['title'];
  $body = $_POST['body'];
  $name = $_POST['name'];
  $uniq = $_POST['uniq']; //user id


  if($_FILES["image"]["name"] != ''){
    $file = explode(".",$_FILES["image"]["name"]);
    $extension  = end($file);
    $new = date("mdYhis");
    $cover_image = $title."@".$new.".".$extension;
    $location = '../coverimg/'.$cover_image;
    move_uploaded_file($_FILES["image"]["tmp_name"],$location);
  }else {
    $cover_image = "noimage.jpg";
  }

  $create = $blog->createPost($uniq,$title,$body,$created_at,$name,$cover_image);

  if($create === true){
    $_SESSION['alert'] = "success";
    //$return['msg'] = "added";
  }else {
    //$return['msg'] = "failed";
    $_SESSION['alert'] = "error";
  }
  //echo json_encode($return);
}//isset

?>
